## AWS Assignment 9

```
Infrastructure as a Code
** Note: Continuing from first assignment **

Task Description 

- Create the following resources using terraform:
    - Create  ec2-instances based on your assigned software(cluster or HA(Use Interpolation))
    - Create one bastion host
    - Create  Security Groups for  your EC2 instances
        - port 22 of bastion host should only accessible from your public ip.
        - port 22 of  the private instances should only be accessible from  bastion host
- Create jenkins pipeline that will create the Infra  
    - There should be  approval stage before the infra is created 
    -  Your job should send notification for status of job in slack.
- Create jenkins pipeline that will destroy the Infra  
    - There should be  approval stage before the infra is Destroyed 
    -  Your job should send notification for status of job in slack.

```


## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_http"></a> [http](#provider\_http) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eip.nat_ip](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_instance.bastion-host](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_instance.redis_host](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_internet_gateway.ninja-igw-01](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_nat_gateway.ninja-nat-01](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway) | resource |
| [aws_route_table.rout-priv](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.rout-pub](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.associate-priv](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.associate-pub](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.bastion-security-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.redis-security-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.private_in](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.private_out](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.public_in_http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.public_in_ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.public_out](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_subnet.terra-ninja-priv-sub](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.terra-ninja-pub-sub](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.terra-ninja-vpc-01](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [http_http.ip](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | ami id | `string` | `"ami-08d4ac5b634553e16"` | no |
| <a name="input_count_ec2_bastion"></a> [count\_ec2\_bastion](#input\_count\_ec2\_bastion) | instance count | `number` | `1` | no |
| <a name="input_count_ec2_redis"></a> [count\_ec2\_redis](#input\_count\_ec2\_redis) | instance count | `string` | `"1"` | no |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | default tags | `map` | <pre>{<br>  "ENV": "testing",<br>  "Owner": "Utkarsh",<br>  "created": "by terraform"<br>}</pre> | no |
| <a name="input_encrypted_volume"></a> [encrypted\_volume](#input\_encrypted\_volume) | instance type | `bool` | `true` | no |
| <a name="input_igw"></a> [igw](#input\_igw) | create IGW | `string` | `"ninja-igw-01"` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | instance type | `string` | `"t2.micro"` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | instance key | `string` | `"nv"` | no |
| <a name="input_nat"></a> [nat](#input\_nat) | create nat | `string` | `"ninja-nat-01"` | no |
| <a name="input_private_ec2_public_ip"></a> [private\_ec2\_public\_ip](#input\_private\_ec2\_public\_ip) | private instance public ip | `bool` | `false` | no |
| <a name="input_public_ip"></a> [public\_ip](#input\_public\_ip) | instance public ip | `bool` | `true` | no |
| <a name="input_redis_host"></a> [redis\_host](#input\_redis\_host) | redis host name | `string` | `"redis_host"` | no |
| <a name="input_redis_security_group_name"></a> [redis\_security\_group\_name](#input\_redis\_security\_group\_name) | security\_group\_name | `string` | `"security_group_redis"` | no |
| <a name="input_region"></a> [region](#input\_region) | availibility zone | `string` | `"us-east-1"` | no |
| <a name="input_rout-priv"></a> [rout-priv](#input\_rout-priv) | create route table priv | `string` | `"ninja-route-priv-01"` | no |
| <a name="input_rout-pub"></a> [rout-pub](#input\_rout-pub) | create route table pub | `string` | `"ninja-route-pub-01"` | no |
| <a name="input_security_group_name"></a> [security\_group\_name](#input\_security\_group\_name) | security\_group\_name | `string` | `"security_group_bastion"` | no |
| <a name="input_subnet_cidr-priv"></a> [subnet\_cidr-priv](#input\_subnet\_cidr-priv) | subnet\_cidr | `list(string)` | <pre>[<br>  "10.0.3.0/24",<br>  "10.0.4.0/24"<br>]</pre> | no |
| <a name="input_subnet_cidr-pub"></a> [subnet\_cidr-pub](#input\_subnet\_cidr-pub) | subnet\_cidr | `list(string)` | <pre>[<br>  "10.0.1.0/24",<br>  "10.0.2.0/24"<br>]</pre> | no |
| <a name="input_tag_bastion_host_sg"></a> [tag\_bastion\_host\_sg](#input\_tag\_bastion\_host\_sg) | bastion\_host\_security\_group | `string` | `"bastion_host_sg"` | no |
| <a name="input_tag_bastion_instance"></a> [tag\_bastion\_instance](#input\_tag\_bastion\_instance) | Create instance | `string` | `"bastion_host"` | no |
| <a name="input_tag_redis_host_sg"></a> [tag\_redis\_host\_sg](#input\_tag\_redis\_host\_sg) | redis host security group | `string` | `"tag_redis_host_sg"` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | size | `number` | `8` | no |
| <a name="input_volume_type"></a> [volume\_type](#input\_volume\_type) | instance type | `string` | `"gp2"` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | vpc cidr | `string` | `"10.0.0.0/16"` | no |
| <a name="input_vpc_tag"></a> [vpc\_tag](#input\_vpc\_tag) | vpc name | `string` | `"terra-ninja-vpc-01"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | availibity zone | `list(string)` | <pre>[<br>  "us-east-1a",<br>  "us-east-1b"<br>]</pre> | no |


- Created jenkins pipeline that will create the Infra  

![](snapshots/jenkins1.png)

 - Add repository in jenkins
    - in respository url

![](snapshots/jenkins2.png)

- Added jenkins file in script path.

![](snapshots/jenkins3.png)

- option add for (procced and abort) in running time on pipeline.

![](snapshots/jenkins4.png)

- Created jenkins pipeline that will destroy the Infra.

![](snapshots/jenkins5.png)

- also added slack notification alert in jenkinsfile.

### DONE.
