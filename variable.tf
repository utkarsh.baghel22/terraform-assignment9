variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "vpc cidr"
}

variable "vpc_tag" {
  type        = string
  default     = "terra-ninja-vpc-01"
  description = "vpc name"
}

variable "subnet_cidr-pub" {
    type = list(string)
    default = ["10.0.1.0/24","10.0.2.0/24"]
    description = "subnet_cidr"
}

variable "subnet_cidr-priv" {
    type = list(string)
    default = ["10.0.3.0/24","10.0.4.0/24"]
    description = "subnet_cidr"
}

variable "zone" {   
    type = list(string)
    default = ["us-east-1a","us-east-1b"]
    description = "availibity zone"
}

variable "igw" {   
    type = string
    default = "ninja-igw-01"
    description = "create IGW"
}

variable "nat" {   
    type = string
    default = "ninja-nat-01"
    description = "create nat"
}

variable "rout-pub" {   
    type = string
    default = "ninja-route-pub-01"
    description = "create route table pub"
}

variable "rout-priv" {   
    type = string
    default = "ninja-route-priv-01"
    description = "create route table priv"
}

variable "security_group_name" {   
    type = string
    default = "security_group_bastion"
    description = "security_group_name"
}

variable "count_ec2_bastion" {   
    type = number
    default = 1
    description = "instance count"
}

variable "ami_id" {   
    type = string
    default = "ami-08d4ac5b634553e16"
    description = "ami id"
}

variable "instance_type" {   
    type = string
    default = "t2.micro"
    description = "instance type"
}

variable "public_ip" {   
    type = bool
    default = true
    description = "instance public ip"
}

variable "key_name" {   
    type = string
    default = "nv"
    description = "instance key"
}

variable "tag_bastion_host_sg" {   
    type = string
    default = "bastion_host_sg"
    description = "bastion_host_security_group"
}

variable "tag_bastion_instance" {   
    type = string
    default = "bastion_host"
    description = "Create instance"
}

variable "volume_size" {   
    type = number
    default = 8
    description = "size"
}

variable "volume_type" {   
    type = string
    default = "gp2"
    description = "instance type"
}

variable "encrypted_volume" {   
    type = bool
    default = true
    description = "instance type"
}

variable "redis_security_group_name" {   
    type = string
    default = "security_group_redis"
    description = "security_group_name"
}

variable "count_ec2_redis" {   
    type = string
    default = "1"
    description = "instance count"
}

variable "private_ec2_public_ip" {   
    type = bool
    default = false
    description = "private instance public ip"
}

variable "tag_redis_host_sg" {   
    type = string
    default = "tag_redis_host_sg"
    description = "redis host security group"
}

variable "redis_host" {   
    type = string
    default = "redis_host"
    description = "redis host name"
}

variable "region" {   
    type = string
    default = "us-east-1"
    description = "availibility zone"
}

variable "default_tags" {   
    type = map
    default = { ENV = "testing", created = "by terraform", Owner = "Utkarsh" }
    description = "default tags"
}
