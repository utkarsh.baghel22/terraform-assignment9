provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}

data "http" "ip" {
  url = "https://ifconfig.me"
}

resource "aws_vpc" "terra-ninja-vpc-01" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  tags = {
    Name = var.vpc_tag
  }
}

resource "aws_subnet" "terra-ninja-pub-sub" {
  vpc_id = aws_vpc.terra-ninja-vpc-01.id
  cidr_block = element(var.subnet_cidr-pub,count.index)
  count = length(var.subnet_cidr-pub)
  availability_zone = element(var.zone,count.index)
  tags = {
    Name = "ninja-pub-sub-${count.index+1}"
  }
}

resource "aws_subnet" "terra-ninja-priv-sub" {
  vpc_id = aws_vpc.terra-ninja-vpc-01.id
  cidr_block = element(var.subnet_cidr-priv,count.index)
  count = length(var.subnet_cidr-priv)
  availability_zone = element(var.zone,count.index)
  tags = {
    Name = "ninja-priv-sub-${count.index+1}"
  }
}

resource "aws_internet_gateway" "ninja-igw-01" {
  vpc_id = aws_vpc.terra-ninja-vpc-01.id

  tags = {
    Name = var.igw
  }
}

resource "aws_eip" "nat_ip" {
  vpc = true
}

resource "aws_nat_gateway" "ninja-nat-01" {
  allocation_id = aws_eip.nat_ip.id
  subnet_id     = aws_subnet.terra-ninja-pub-sub[0].id
  tags = {
    Name = var.nat
  }
}

resource "aws_route_table" "rout-pub" {
  vpc_id = aws_vpc.terra-ninja-vpc-01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ninja-igw-01.id
  }

  tags = {
    Name = var.rout-pub
  }
}

resource "aws_route_table" "rout-priv" {
  vpc_id = aws_vpc.terra-ninja-vpc-01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ninja-nat-01.id
  }

  tags = {
    Name = var.rout-priv
  }
}

resource "aws_route_table_association" "associate-pub" {
  count = length(aws_subnet.terra-ninja-pub-sub.*.id)
  subnet_id = element(aws_subnet.terra-ninja-pub-sub.*.id,count.index)
  route_table_id = aws_route_table.rout-pub.id
}
resource "aws_route_table_association" "associate-priv" {
  count = length(aws_subnet.terra-ninja-priv-sub.*.id) 
  subnet_id = element(aws_subnet.terra-ninja-priv-sub.*.id,count.index)
  route_table_id = aws_route_table.rout-priv.id
}
resource "aws_security_group" "bastion-security-group" {
  name = var.security_group_name
  description = "Public internet access"
  vpc_id =  aws_vpc.terra-ninja-vpc-01.id
 
  
  tags = {
    Name = var.tag_bastion_host_sg
  }

}

resource "aws_security_group_rule" "public_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.bastion-security-group.id
}
 
resource "aws_security_group_rule" "public_in_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "ssh"
  cidr_blocks       = [join("/", [data.http.ip.body, "32"])]
  security_group_id = aws_security_group.bastion-security-group.id
}
 
resource "aws_security_group_rule" "public_in_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "http"
  cidr_blocks       = [join("/", [data.http.ip.body, "32"])]
  security_group_id = aws_security_group.bastion-security-group.id
}

resource "aws_instance" "bastion-host" {
 count                        = var.count_ec2_bastion
  ami                         = var.ami_id
  instance_type               = var.instance_type
  associate_public_ip_address = var.public_ip
  key_name                    = var.key_name
  availability_zone           = element(var.zone,count.index)
  subnet_id                   = element(aws_subnet.terra-ninja-pub-sub.*.id,count.index)
  vpc_security_group_ids      = [aws_security_group.bastion-security-group.id]
  root_block_device {
    volume_size = var.volume_size
    volume_type = var.volume_type
    encrypted   = var.encrypted_volume
  }

  tags = {
    Name = var.tag_bastion_instance
  }
}

resource "aws_security_group" "redis-security-group" {
  name = var.redis_security_group_name
  description = "Private internet access"
  vpc_id = aws_vpc.terra-ninja-vpc-01.id

  tags = {
    Name = var.tag_redis_host_sg
  }
}
 
resource "aws_security_group_rule" "private_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.redis-security-group.id
}
 
resource "aws_security_group_rule" "private_in" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "ssh"
  cidr_blocks = [aws_vpc.terra-ninja-vpc-01.cidr_block]
  security_group_id = aws_security_group.redis-security-group.id
}

resource "aws_instance" "redis_host" {
  count                        = var.count_ec2_redis
  ami                         = var.ami_id
  instance_type               = var.instance_type
  associate_public_ip_address = var.private_ec2_public_ip
  key_name                    = var.key_name
  availability_zone           = element(var.zone,count.index)
  subnet_id                   = element(aws_subnet.terra-ninja-priv-sub.*.id,count.index)
  vpc_security_group_ids      = [aws_security_group.redis-security-group.id]
  root_block_device {
    volume_size = var.volume_size
    volume_type = var.volume_type
    encrypted   = var.encrypted_volume
  }

  tags = {
    Name = "${var.redis_host}-${count.index+1}"
  }
}